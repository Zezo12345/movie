
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:movie/home/data/model/GetMovieResponse.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'RestClient.g.dart';
@JsonSerializable()
//@RestApi(baseUrl: "connextst.ebtekarcloud.com/external-api")
//@RestApi(baseUrl: "http://206.189.28.185:9000/ragab_service/")
@RestApi(baseUrl: "https://api.themoviedb.org/")
 // parser: Parser.FlutterCompute)
abstract class RestClient {
  factory RestClient( Dio dio,{ String baseUrl }) = _RestClient;

  @GET('3/discover/movie?api_key={api_key}&page={page}')
  Future<GetMovieResponse> getMovies(@Path("api_key") String? api_key,@Path("page") int? page
      );

}
