import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movie/home/presentation/bloc/home_event.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home/presentation/FilmsPage.dart';
import 'home/presentation/bloc/home_bloc.dart';
import 'injection_container.dart' as di;
import 'package:logger/logger.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'network/RestClient.dart';

final logger = Logger();
Dio dio = Dio();
final client = RestClient(dio);
late String subscriptionToken;
late SharedPreferences sharedPreferences;
void main() async {
  dio.interceptors.add(PrettyDioLogger());
// customization
  dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
  WidgetsFlutterBinding.ensureInitialized();
  sharedPreferences = await SharedPreferences.getInstance();

  ///Here should call api for getting subscriptionToken from Enas
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (_) => di.sl<HomeBloc>()..add(const LoadMovieEvent(1))),
        ],
        child: ScreenUtilInit(
            designSize: const Size(360, 690),
            minTextAdapt: true,
            splitScreenMode: true,
            builder: (context, child) {
              return MaterialApp(
                title: 'Flutter Demo',
                theme: ThemeData(
                  colorScheme:
                      ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                  useMaterial3: true,
                ),
                home: const FilmsPage(),
              );
            }));
  }
}
