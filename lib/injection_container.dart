import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'home/domain/repositories/home_repository.dart';
import 'home/presentation/bloc/home_bloc.dart';
import 'main.dart';

final sl = GetIt.instance;

Future<void> init() async {



  sl.registerFactory(() => HomeBloc());

  sl.registerLazySingleton(() => HomeRepository(sl()));


  sl.registerLazySingleton(() => InternetConnectionChecker());




}
