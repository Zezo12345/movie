import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {}

class OfflineFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class ServerFailure extends Failure {
  final String message;

  ServerFailure(this.message);
  @override
  List<Object?> get props => [message];
}

class EmptyCacheFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class VerificationFailure extends Failure {
  final String message;

  VerificationFailure(this.message);
  @override
  List<Object?> get props => [message];
}

class PhoneOrPasswordFailure extends Failure {
  final String message;

  PhoneOrPasswordFailure(this.message);
  @override
  List<Object?> get props => [message];
}
