import 'package:flutter/material.dart';

class AppColors{
  static const Color whiteColor = Colors.white;
  static const Color blackColor = Colors.black;
  static const Color limeColor = Colors.lime;
  static const Color lightBlack = Color(0xff2F2C2C);
}