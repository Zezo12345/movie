import 'package:dartz/dartz.dart';
import 'package:movie/home/data/model/GetMovieResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../core/error/failures.dart';

abstract class HomeRepository {
  factory HomeRepository(String responseType) {
    if (responseType == 'getMovieResponse') return GetMovieResponse();

    throw 'Can\'t create $responseType.';
  }
  Future<Either<Failure, HomeRepository>> getObj(int page);
}
