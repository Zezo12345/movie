import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movie/core/appColors.dart';
import 'package:movie/home/data/model/MovieModel.dart';

class ProductDetailsPage extends StatelessWidget {
  const ProductDetailsPage({required this.movieModel, super.key});
  final MovieModel movieModel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //1
      backgroundColor: AppColors.lightBlack,
      body: CustomScrollView(
        primary: true,
        slivers: <Widget>[
          //2
          SliverAppBar(
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(movieModel.title ?? '', textScaleFactor: 1),
              background: Image.network(
                'https://media.themoviedb.org/t/p/w220_and_h330_face/${movieModel.poster_path}',
                fit: BoxFit.fill,
              ),
            ),
          ),
          SliverList.list(
            children: [
              ListTile(
                title: Text(
                  movieModel.original_title ?? '',
                  style:
                      TextStyle(fontSize: 20.sp, color: AppColors.whiteColor),
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movieModel.release_date ?? '',
                      style: TextStyle(
                          fontSize: 13.sp, color: AppColors.whiteColor),
                    ),
                    Text(
                      "${movieModel.vote_average} / ${movieModel.vote_count}",
                      style: TextStyle(
                          fontSize: 13.sp, color: AppColors.whiteColor),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'Summary',
                style: TextStyle(fontSize: 20.sp, color: AppColors.whiteColor),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                    child: Text(
                  movieModel.overview ?? '',
                  style:
                      TextStyle(fontSize: 20.sp, color: AppColors.whiteColor),
                )),
              ),
              Container(
                height: 250,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://media.themoviedb.org/t/p/w220_and_h330_face/${movieModel.backdrop_path}'))),
              )
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: AppColors.limeColor,
        child: const Icon(
          Icons.heart_broken_outlined,
          color: AppColors.whiteColor,
        ),
      ),
    );
  }
}
