import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movie/core/appColors.dart';
import 'package:movie/home/data/model/MovieModel.dart';
import 'package:movie/home/presentation/bloc/home_bloc.dart';
import 'package:movie/home/presentation/bloc/home_event.dart';
import 'package:movie/home/presentation/bloc/home_state.dart';
import '../../core/util/message_display_widget.dart';
import 'cards/home_category_card.dart';

class FilmsPage extends StatefulWidget {
  const FilmsPage({super.key});

  @override
  State<FilmsPage> createState() => _FilmsPageState();
}

class _FilmsPageState extends State<FilmsPage> {
  final _scrollController = ScrollController();
  int page = 0;
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  Future<void> _refresh() async {
    setState(() {
      movies.clear();
    });
    context.read<HomeBloc>().add(LoadMovieEvent(1));
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    if (currentScroll >= (maxScroll * 0.9)) {
      setState(() {
        page++;
      });
      context.read<HomeBloc>().add(LoadMovieEvent(page));
    }
  }

  int currentIndex = 0;
  List<MovieModel> movies = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text("Films",style: TextStyle(color: AppColors.whiteColor,fontSize: 22.sp),),
          centerTitle: true,
          backgroundColor: AppColors.blackColor,
        ),
        body:SafeArea(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Wrap(
              children: [
                BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
                  if (state is LoadingHomeState) {
                    return const Center(
                      child: SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          color: Colors.yellowAccent,
                        ),
                      ),
                    );
                  } else if (state is LoadedHomeState) {
                    movies.addAll(state.getMovieResponse.results ?? []);
                    return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: RefreshIndicator(
                        onRefresh: _refresh,
                        child: GridView.builder(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  // maxCrossAxisExtent: 200,
                                  childAspectRatio: .55,
                                  crossAxisSpacing: 0,
                                  mainAxisSpacing: 0,
                                  crossAxisCount: 2),
                          itemCount: movies.length,
                          controller: _scrollController,
                          itemBuilder: (BuildContext context, int index) {
                            currentIndex = index;
                            return HomeCategoryCard(
                                movieModel: movies[index], index: index);
                          },
                        ),
                      ),
                    );
                  } else if (state is ErrorHomeState) {
                    return MessageDisplayWidget(message: state.message);
                  }
                  return const SizedBox();
                }),
                currentIndex > 0 && currentIndex % 20 == 0
                    ? const Center(
                        child:  CircularProgressIndicator(
                          color: Colors.yellowAccent,
                        ),
                      )
                    : const SizedBox.shrink()
              ],
            ),
          ),
        ));
  }
}
