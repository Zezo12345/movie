import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../data/model/MovieModel.dart';
import '../ProductDetailsPage.dart';

class HomeCategoryCard extends StatelessWidget {
  const HomeCategoryCard(
      {required this.index, required this.movieModel, Key? key})
      : super(key: key);

  final MovieModel movieModel;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Theme.of(context).colorScheme.outline,
        ),
      ),
      elevation: 3,
      margin: EdgeInsets.zero,
      color: Colors.lime[700],
      child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductDetailsPage(
                          movieModel: movieModel,
                        )));
          },
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        "https://media.themoviedb.org/t/p/w220_and_h330_face/${movieModel.poster_path}"),
                    fit: BoxFit.fill)),
            child: Stack(
              alignment: Alignment.bottomCenter,
              fit: StackFit.loose,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: 300.w,
                  color: index % 2 == 0
                      ? Colors.red.withOpacity(.6)
                      : Colors.lime.withOpacity(.6),
                  height: 60,
                  child: Text(
                    movieModel.title ?? '',
                    style: TextStyle(fontSize: 18.sp, color: Colors.white),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
