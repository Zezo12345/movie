import 'package:equatable/equatable.dart';

import '../../data/model/GetMovieResponse.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class LoadingHomeState extends HomeState {}

class LoadedHomeState extends HomeState {
  final GetMovieResponse getMovieResponse;

  const LoadedHomeState({required this.getMovieResponse});

  @override
  List<Object> get props => [getMovieResponse];
}

class ErrorHomeState extends HomeState {
  final String message;

  const ErrorHomeState({required this.message});

  @override
  List<Object> get props => [message];
}
