import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  final int page;
  const HomeEvent(this.page);

  @override
  List<Object> get props => [];
}

class LoadMovieEvent extends HomeEvent {
  const LoadMovieEvent(super.page);
}

class RefreshCategoriesEvent extends HomeEvent {
  const RefreshCategoriesEvent(super.page);
}
