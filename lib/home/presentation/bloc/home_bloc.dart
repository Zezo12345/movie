import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/home/data/model/GetMovieResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../core/error/failures.dart';
import '../../../core/strings/failures.dart';
import '../../domain/repositories/home_repository.dart';
import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
 HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>((event, emit) async {
      emit(LoadingHomeState());
      final failureOrDepartments =
          await HomeRepository('getMovieResponse').getObj(event.page);
      emit(_mapFailureOrPostsToState(failureOrDepartments));
    });
  }

  HomeState _mapFailureOrPostsToState(Either<Failure, HomeRepository> either) {
    return either.fold(
      (failure) => ErrorHomeState(message: _mapFailureToMessage(failure)),
      (getMovieResponse) => LoadedHomeState(
        getMovieResponse: getMovieResponse as GetMovieResponse,
      ),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return failure.toString();
      case EmptyCacheFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return failure.toString();
    }
  }
}
