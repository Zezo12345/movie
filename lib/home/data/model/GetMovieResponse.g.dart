// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetMovieResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetMovieResponse _$GetMovieResponseFromJson(Map<String, dynamic> json) =>
    GetMovieResponse(
      page: json['page'] as int?,
      results: (json['results'] as List<dynamic>?)
          ?.map((e) => MovieModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetMovieResponseToJson(GetMovieResponse instance) =>
    <String, dynamic>{
      'page': instance.page,
      'results': instance.results,
    };
