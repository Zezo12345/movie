import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:movie/core/error/failures.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../main.dart';
import '../../domain/repositories/home_repository.dart';
import 'MovieModel.dart';
part 'GetMovieResponse.g.dart';

@JsonSerializable()
class GetMovieResponse extends Equatable implements HomeRepository {
  late int? page;
  late List<MovieModel>? results;

  GetMovieResponse({this.page, this.results});

  factory GetMovieResponse.fromJson(Map<String, dynamic> json) =>
      _$GetMovieResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetMovieResponseToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [page, results];

  @override
  getObj(page) async {
    try {
      final GetMovieResponse ret =
          await client.getMovies("2001486a0f63e9e4ef9c4da157ef37cd", page);
      return Right(ret);
    } catch (e) {
      return Left(ServerFailure(e.toString()));
    }
  }
}
